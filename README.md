# pglogical for Windows

## Overview

This repository contains a port of pglogical 2.4.4 for Windows. The `src` subdirectory contains a Visual Studio 2015 project that you can quickly compile into the two relevant DLL files, `pglogical.dll` and `pglogical_output.dll`. However, the DLL files have already been built for the x64 platform.

A small batch script is provided to install the DLLs on your database servers. A bundled pglmanager utility allows basic control over creating and manipulating a pglogical-based replication network.

## Quick Terminology

pglogical uses *logical replication*, an advanced replication scheme that is relatively new in PostgreSQL. Compared to traditional trigger-based backups, logical replication is more tightly integrated with PostgreSQL's write-ahead logging system, or WAL. Much of the technology is now integrated into PostgreSQL without extra libraries, but the pglogical package still has some enhanced functionality that makes it worth using instead.

Typically pglogical is designed for one-way backup (primary to secondary), but it can be used for bi-directional replication in very basic scenarios.

If a collection of computers each has a copy of the same database, each system's instance of the database is known as a *node* in pglogical terms. A node whose changes are shared with other nodes is known as a *provider* (another term used is *publisher*). A node which accepts changes from other nodes is known as a *subscriber*. Nodes are both subscribers and providers for bi-directional replication.

We'll extend the vocabulary by referring to nodes that are used to manipulate the database in the field as *worker nodes*. All worker nodes then become providers to a *parent node*, a special system which is a subscriber to each worker node. It handles conflicts and aggregates all changes. The worker nodes then subscribe to the parent node to obtain aggregated changes from all other worker nodes, making every node in the network both a subscriber and provider.

A worker node is a provider, and it has exactly one subscription to the parent node. A parent node has subscription to all worker nodes, and it is also a provider to all the nodes, as well.

## Installing

### Requirements

* Python 3.x
  * psycopg2 module
  * lxml module
* PostgreSQL 14
* Windows Firewall must allow PostgreSQL (port 5432) connections from the LAN.
* The same username and password are set for all nodes.

### Configuration

Each computer must be configured with the pglogical extension and have the correct server settings to run.

* Download or clone this repository to any destination computer.
* If you are on the parent node, edit the `postgresql.conf` file provided in the repository.
    * Change `max_worker_processes` to the number of worker nodes (overestimate).
    * Change `max_replication_slots` to the number of worker nodes (overestimate).
    * Change `max_wal_sender` to the number of worker nodes (overestimate).
* If you are not on the parent node, the settings are OK as is.
* Edit the `pg_hba.conf` file and ensure that the network settings allow a LAN connection into the system. `192.168.1.0/24` has been inserted, but this should be adjusted for your network.
* Right click on the `applyChanges.bat` and select "Run as Administrator." This will stop the PostgreSQL server, copy in the new DLL and support files for pglogical into the server directory, and then restart the server.

At this point pglogical is fully functional and can be used as an extension.

### Database Setup

In order to take advantage of the bundled utility and simplified setup, the database name across nodes *must be the same*. The utility will not work properly without it.

* Create your database and tables on each node. You don't have to synchronize the data; pglogical can do that for you as long as the data is on the parent node.

* **Your application is responsible for altering sequences for offsetting.** You either need to do this with SQL commands or ensure your program handles this.

### Initializing pglogical network

* Ensure that your systems are all connected to the network.

* On either one of the node systems or another system that can reach the others, open a Windows Powershell or Command Prompt in the `pglmanager` subdirectory.

* Edit the `pglogical_nodes.xml` to reflect your network. The `Parent` entry defines the parent node, and the `Worker` tags list all workers. Ensure the other settings match your database and credentials.

* Type `python pglmanager.py --testnodes` to ensure everything is reachable. If any node fails, it is important to correct the connection issues.

* Once you're sure things are working, type `python pglmanager.py --buildnodes`. All nodes will be set with pglogical enabled for the database.

* Subscribe the workers to the parent. If you have the database already loaded on the parent, you can synchronize the data with `python pglmanager.py --subscribeandsyncworker` to force an initial one-time copy to worker nodes.
    * This step may take a while for large databases.
    * If you don't want to do the sync from the parent and just want to subscribe workers, `python pglmanager.py --subscribeworker` will do the trick.

* Once you have *ensured* all nodes have synchronized, subscribe the parent node to workers with `python pglmanager.py --subscribeparent`.

The network is now initialized. 

### Common Tasks

* You can disable and enable subscriptions to toggle whether systems should try and sync changes. There are four main options:
    * `--enableworkersubs` directs a worker node to enable its subscription to the parent. Once enabled, changes are automatically pulled from the parent on an ongoing basis until disabled.
    * `--disableworkersubs` tells a worker node to disable pulling changes from the parent until it is re-enabled. However, any changes the worker makes are still pushed to the parent (unless `--disableparentsubs` has been run).
    * `--disableparentsubs` tells the parent to stop pulling changes from workers. However, workers can still pull existing changes from the parent (unless `--disableworkersubs` has been run).
    * `--enableparentsubs` reverse the above and pulls changes on an ongoing basis until directed otherwise.
* The above commands can be given a space-separated list of worker nodes on which to apply these settings. If no list is supplied it is assumed all worker nodes should be affected.

#### Examples

`python pglmanager.py --disableworkersubs blue black`

will disable subscriptions to the parent on nodes blue and black, but no others.

`python pglmanager.py --disableparentsubs`

will disable all subscriptions to worker nodes on the parent node.

### Logging

Useful log information is created in the `PostgreSQL\14\data\log` subdirectory and appended to normal log output. The majority of errors and conflict resolutions are found here.
