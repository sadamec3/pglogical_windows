cd /D "%~dp0"

sc stop postgresql-x64-14 

SETLOCAL

set PGPASSWORD=Electronics2
set adminUser=postgres

set "postgresDir=C:\Program Files\PostgreSQL\14"
set "postgresLibDir=%postgresDir%\lib"
set "postgresSharedDir=%postgresDir%\share\extension"
set "postgresDataDir=%postgresDir%\data"

copy *.sql "%postgresSharedDir%"
copy pglogical.control "%postgresSharedDir%"
copy pglogical.dll "%postgresLibDir%"
copy pglogical_output.dll "%postgresLibDir%"
copy postgresql.conf "%postgresDataDir%"
copy pg_hba.conf "%postgresDataDir%"

sc start postgresql-x64-14

set /p DUMMY=Hit ENTER to continue...
