import os
import argparse
import sys
import lxml.etree as ET
import psycopg2 as pg2
import traceback
import subprocess

# A small and very simple utility to front-end pglogical's replication control commands.

# The file containing some SQL commands useful to pglogical.
commandFile = 'pglogical_commands.xml'

# A list of the nodes in the network.
nodeFile = 'pglogical_nodes.xml'

# Stores information about workers (non-parent nodes) in the network.
workerNodes = {}
# Stores parsed commands from command XML.
commands = {}
# Stores information about the parent node.
parentNode = {}
# Name of database used by all nodes for replication.
dbname = None
# Name of user which accesses all databases.
username = None
# Password for user.
password = None
# Global connection variable used by functions.
conn = None
# Whether more output should be sent to screen.
verbose = False

## Gets a parameter from kwargs or raises an Exception.
def _getParameterOrDie(kwargs, parameter):
    if kwargs.get(parameter, None) is None:
        msg = "Missing argument to connection: %s" % parameter
        raise ValueError(msg)
    return kwargs.get(parameter)

# If verbosity is enabled, print a message.
def _message(msg):
    if verbose:
        print(msg)

# Connects to a PostgreSQL database.
def connectToServer(**kwargs):
    global password
    global username
    global conn
    global dbname
    hostname = _getParameterOrDie(kwargs, 'hostname')
    try:
        conn = pg2.connect(dbname=dbname, user=username,
                           host=hostname, port=5432,
                           password=password)
    except pg2.Error as perr:
        print("Connection failed, reason: %s" % str(perr))
        print("Program will abort.")
        sys.exit(1) 

    _message("Successfully connected to %s as user %s for database %s" % (hostname, username, dbname))

# Closes PostgreSQL database connection.
def closeConnection():
    global conn
    conn.close()
    _message("Connection closed.")
    conn = None

# Parses commands XML.
def parse_commands():
    global commands
    if not os.path.isfile(commandFile):
        raise ValueError("Missing command file from application")
    tree = ET.parse(commandFile)
    root = tree.getroot()
    if root.tag != "PGLogical":
        raise ValueError("Command XML file does not contain PGLogical tag")
    for commandElem in root.findall('Command'):
        commandName = commandElem.attrib.get('name', None)
        if commandName is None:
            msg = "A command declared in the file is missing a name"
            raise ValueError(msg)
        commands[commandName] = commandElem.text.strip()

# Parses nodes XML.
def parse_nodes():
    global workerNodes
    global parentNode
    global username
    global password
    global dbname
    if not os.path.isfile(nodeFile):
        raise ValueError("Missing node file from application")
    tree = ET.parse(nodeFile)
    root = tree.getroot()
    if root.tag != "PGLogical":
        raise ValueError("Node XML file does not contain PGLogical tag")
    username = root.find('User').text.strip()
    password = root.find('Password').text.strip()
    dbname = root.find('Database').text.strip()
    parentElem = root.find('Parent')
    parentNode['Name'] = parentElem.find('Name').text
    parentNode['Host'] = parentElem.find('Host').text
    for workerElem in root.findall('Worker'):
        name = workerElem.find('Name').text
        workerNodes[name] = {}
        workerNodes[name]['Host'] = workerElem.find('Host').text

## Executes an SQL query.
#  @arg sqlStr Query to execute.
#  @arg needCursor Whether you need the cursor after the function call.  If
#                  True, the cursor is returned. If not, it is closed. You
#                  may need a cursor to fetch results.
def runQuery(sqlStr, needCursor=False):
    global conn
    cursor = conn.cursor()
    try:
        _message(sqlStr)
        cursor.execute(sqlStr)
    except pg2.Error as e:
        print("Problem executing SQL query")
        print("Query: %s" % sqlStr)
        print("Error: %s" % str(e))
        raise
    else:
        if needCursor:
            return cursor
        conn.commit()

## Executs a list of SQL queries on the parent node.
def runOnParent(cmdList):
    hostname = parentNode['Host']
    connectToServer(hostname=hostname)
    for cmd in cmdList:
        runQuery(cmd)
    closeConnection()

## Executes a list of SQL queries on a worker node with a specific name.
def runOnWorker(name, cmdList):
    connectToServer(hostname=workerNodes[name]['Host'])
    for cmd in cmdList:
        runQuery(cmd)
    closeConnection()
   
## Executes a list of SQL queries on all worker nodes.
def runOnWorkers(cmdList):
    for worker in workerNodes.keys():
        runOnWorker(worker, cmdList)

## Returns a modified SQL command to create a node based on the node
#  information parsed in the XML.
#  @arg nodeName Name of the worker node to create.
#  @return SQL query string to execute on node for creation.
def nodeCreationString(nodename):
    nodeStr = commands['createNode']
    nodeStr = nodeStr.replace('@NODENAME@', nodename)
    nodeStr = nodeStr.replace('@USER@', username)
    nodeStr = nodeStr.replace('@PASSWORD@', password)
    nodeStr = nodeStr.replace('@DATABASE@', dbname)
    return nodeStr

## Returns a modified SQL command to create a subscription based on the node
#  information parsed in the XML.
#  @arg nodeName Name of the non-parent node involved in the subscription.
#  @arg onParent Boolean indicating whether this is a subscription created on
#                the parent (meaning pull changes from worker) or not (pull
#                changes from parent)
#  @arg forceSync Forces the data and structure on the subscriber to mimic that
#                 of the provider.
#  @return The SQL query string to create the subscription.
def subCreationString(nodename, onParent, forceSync=False):
    if forceSync:
        forceSync = 'true'
    else:
        forceSync = 'false'
    if not onParent:
        sourceNode = 'parent'
        destNode = nodename
        hostname = parentNode['Host']
        forward = 'all'
    else:
        sourceNode = nodename
        hostname = workerNodes[sourceNode]['Host']
        destNode = 'parent'
        forward = ''
    replaceMap = { 'SOURCE': sourceNode,
                   'DEST': destNode,
                   'HOSTNAME': hostname,
                   'DATABASE': dbname,
                   'USER': username,
                   'PASSWORD': password,
                   'FORWARD': forward,
                   'FORCESYNC': forceSync
                   }
    subCreationString = commands['createSubscription']
    for old, new in replaceMap.items():
        subCreationString = subCreationString.replace('@' + old + '@', new)
    return subCreationString
    
def syncCreationString(nodename):
    
    replaceMap = { 'SUBNAME': 'from_parent_to_%s' % nodename }
    syncCreationString = commands['syncSubscription']
    for old, new in replaceMap.items():
        syncCreationString = syncCreationString.replace('@' + old + '@', new)
    return syncCreationString

## Builds all nodes in the network from scratch.
def buildNodes():
    runOnParent((commands['createExtension'],
                 nodeCreationString(parentNode['Name'])))
    for worker in workerNodes.keys():
        runOnWorker(worker, (commands['createExtension'],
                             nodeCreationString(worker)))

## Wipes all nodes on the network. This completely removes all traces
#  on both the parent and the workers.
def wipeNodes():
    dropCommand = commands['dropExtension']
    runOnParent((dropCommand,))
    runOnWorkers((dropCommand,))

def subscribeWorkersToParent(workerList, forceSync=False):
    if len(workerList) == 0:
        workerList = workerNodes.keys()
    for worker in workerNodes:
        if worker not in workerList:
            continue
        runOnWorker(worker, (subCreationString(worker, onParent=False,
                                               forceSync=forceSync),))
                                               
def syncWorkersToParent(workerList):
    if len(workerList) == 0:
        workerList = workerNodes.keys()
    for worker in workerNodes:
        if worker not in workerList:
            continue
        runOnWorker(worker, (syncCreationString(worker),))                             

def subscribeParentToWorkers(workerList):
    if len(workerList) == 0:
        workerList = workerNodes.keys()
    for worker in workerNodes:
        if worker not in workerList:
            continue
        runOnParent((subCreationString(worker, onParent=True),))
        
## Lists all nodes in the network.
def listNodes():
    print("Parent (master) node: %s, host %s" % (parentNode['Name'], parentNode['Host']))
    for name, vals in workerNodes.items():
        print("%s, host %s" % (name, vals['Host']))

## Tests whether a PostgreSQL server at a particular hostname is reachable.
#  @return (Boolean) whether it can be reached.
def canReachNode(hostname):
    try:
        subprocess.check_call("pg_isready -q -t 3 -h %s" % (hostname))
    except subprocess.CalledProcessError:
        return False
    else:
        return True

## Tests whether the parent node is reachable.
#  @return (Boolean) whether it can be reached.
def canReachParent():
    return canReachNode(parentNode['Host'])

## Tests that parent and worker nodes can be reached.
def testNodes():
    failed = 0
    passed = 0
    
    sys.stdout.write("Testing parent node: ")
    sys.stdout.flush()
    if canReachParent():
        sys.stdout.write("passed.\n")
        sys.stdout.flush()
        passed += 1 
    else:
        sys.stdout.write("failed.\n")
        sys.stdout.flush()
        failed += 1
    for name, vals in workerNodes.items():
        host = vals['Host']
        sys.stdout.write("Testing node %s (%s): " % (name, host))
        sys.stdout.flush()
        if canReachNode(host):
            sys.stdout.write("passed.\n")
            sys.stdout.flush()
            passed += 1
        else:            
            sys.stdout.write("failed.\n")
            sys.stdout.flush()
            failed += 1

    print("%d passed, %d failed" % (passed, failed))

## Fetches all subscription names for a given host.
#  @arg hostname Hostname to connect with for subscriptions
#  @return (list) Subscription names
def getSubscriptions(hostname):
    connectToServer(hostname=hostname)
    subs = []
    getSubs = commands['getSubscriptions']
    c = runQuery(getSubs, needCursor=True)
    rows = c.fetchall()
    for row in rows:
        subs.append(row[0])
    c.close()
    closeConnection()
    return subs

## Fetches all subscription names on the parent.
#  @return (list) Subscription names on parent.   
def getParentSubs():
    return getSubscriptions(parentNode['Host'])

## Fetches all subscription names on a worker node.
#  @arg name Name of worker node.
#  @return (list) Subscription names on worker node. 
def getNodeSubs(name):
    return getSubscriptions(workerNodes[name]['Host'])
 
## Enables subscription to parent on some or all worker nodes.
#  @arg workerList List of workers on which to enable subscription. If the
#                  list is empty, all workers are changed.
def enableWorkerSubs(workerList):
    if len(workerList) == 0:
        workerList = workerNodes.keys()
    for worker in workerNodes:
        if worker not in workerList:
            continue
        wSubs = getSubscriptions(workerNodes[worker]['Host'])
        enableCmd = commands['enableSubscription']
        cmds = []
        for aSub in wSubs:
            cmds.append(enableCmd.replace('@SUBNAME@', aSub))
        runOnWorker(worker, cmds)

## Disables subscription to parent on some or all worker nodes.
#  @arg workerList List of workers on which to disable subscription. If the
#                  list is empty, all workers are changed.    
def disableWorkerSubs(workerList):
    if len(workerList) == 0:
        workerList = workerNodes.keys()
    for worker in workerNodes:
        if worker not in workerList:
            continue
        wSubs = getSubscriptions(workerNodes[worker]['Host'])
        disableCmd = commands['disableSubscription']
        cmds = []
        for aSub in wSubs:
            cmds.append(disableCmd.replace('@SUBNAME@', aSub))
        runOnWorker(worker, cmds)

## Enables subscription to some or all worker nodes on the parent node.
#  @arg workerList List of workers from which the parent should enable
#                  subscription. If the list is empty, all workers are
#                  enabled.           
def enableParentSubs(workerList):
    pSubs = getParentSubs()
    enableCmd = commands['enableSubscription']
    cmds = []
    for aSub in pSubs:
        if len(workerList) > 0:
            sourceName = aSub.split('_')[1]
            if sourceName not in workerList:
                continue
        cmds.append(enableCmd.replace('@SUBNAME@', aSub))
    runOnParent(cmds)

## Disables subscription to some or all worker nodes on the parent node.
#  @arg workerList List of workers from which the parent should disable
#                  subscription. If the list is empty, all workers are
#                  disabled.     
def disableParentSubs(workerList):
    pSubs = getParentSubs()
    disableCmd = commands['disableSubscription']
    cmds = []
    for aSub in pSubs:
        if len(workerList) > 0:
            sourceName = aSub.split('_')[1]
            if sourceName not in workerList:
                continue
        cmds.append(disableCmd.replace('@SUBNAME@', aSub))
    runOnParent(cmds)

## Validates that nodes specified on the command line actually exist.    
def validateNodeList(aList):
    for node in aList:
        if node not in workerNodes:
            raise ValueError("Error: node %s is not registered in the network" % node)
    
if __name__ == '__main__':
    try:
        parser = argparse.ArgumentParser(description='Manages PGLogical replication')

        group = parser.add_mutually_exclusive_group(required=True)
        group.add_argument('--buildnodes', action='store_true',
                           help='Builds all nodes (parent and workers) in the network')
        group.add_argument('--subscribeworker', nargs='*',
                           help='Subscribes worker(s) to parent without synchronization')
        group.add_argument('--subscribeandsyncworker', nargs='*',
                           help='Subscribes worker(s) to parent with synchronization')
        group.add_argument('--syncworker', nargs='*',
                           help='Synchronizes worker(s) to parent')
        group.add_argument('--subscribeparent', nargs='*',
                           help='Subscribes parents to worker(s)')
        group.add_argument('--listnodes', action='store_true',
                           help='List the nodes available')
        group.add_argument('--testnodes', action='store_true',
                           help='Test node connectivity')
        group.add_argument('--enableworkersubs', nargs='*',
                           help='Enable all subscriptions on workers (resume getting changes from parent)')
        group.add_argument('--disableworkersubs', nargs='*',
                           help='Disable all subscriptions on workers (stop getting changes from parent')
        group.add_argument('--enableparentsubs', nargs='*',
                           help='Enable subscriptions on parent (resume getting changes from workers)')
        group.add_argument('--disableparentsubs', nargs='*',
                           help='Disable subscriptions on parent (stop getting changes from workers)') 
        group.add_argument('--wipenodes', action='store_true')                           
        parser.add_argument('-v', '--verbose', action='store_true',
                           help='Extra verbosity')
        args = parser.parse_args()
        verbose = True
        parse_commands()
        parse_nodes()
        if args.buildnodes:
            print("Building initial nodal network...")
            wipeNodes()
            buildNodes()
            print("Done.")
        elif args.subscribeworker is not None:
            workerList = args.subscribeworker
            if len(workerList) > 0:
                validateNodeList(workerList)
            subscribeWorkersToParent(workerList)
        elif args.subscribeandsyncworker is not None:
            workerList = args.subscribeandsyncworker
            if len(workerList) > 0:
                validateNodeList(workerList)
            subscribeWorkersToParent(workerList, forceSync=True)  
        elif args.syncworker is not None:
            workerList = args.syncworker
            if len(workerList) > 0:
                validateNodeList(workerList)
            syncWorkersToParent(workerList)         
        elif args.subscribeparent is not None:
            workerList = args.subscribeparent
            if len(workerList) > 0:
                validateNodeList(workerList)
            subscribeParentToWorkers(workerList)          
        elif args.wipenodes:
            wipeNodes()
        elif args.listnodes:
            listNodes()
        elif args.testnodes:
            testNodes()
        elif args.enableworkersubs is not None:
            workerList = args.enableworkersubs
            if len(workerList) > 0:
                validateNodeList(workerList)
            enableWorkerSubs(workerList)
        elif args.disableworkersubs is not None:
            workerList = args.disableworkersubs
            if len(workerList) > 0:
                validateNodeList(workerList)
            disableWorkerSubs(workerList)
        elif args.enableparentsubs is not None:
            workerList = args.enableparentsubs
            if len(workerList) > 0:
                validateNodeList(workerList)
            enableParentSubs(workerList)
        elif args.disableparentsubs is not None:
            workerList = args.disableparentsubs
            if len(workerList) > 0:
                validateNodeList(workerList)
            disableParentSubs(workerList)
            
            
    except Exception as e:
        traceback.print_exc()
    finally:
        print()
        input("Press ENTER to exit...")


